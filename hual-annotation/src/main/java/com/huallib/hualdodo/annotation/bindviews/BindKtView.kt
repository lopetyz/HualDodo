package com.huallib.hualdodo.annotation.bindviews

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.SOURCE)
annotation class BindKtView(val id: Int)