package com.huallib.hualdodo.annotation.router

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class Route(
    val path: String,
    val exported: Boolean
)
