package com.huallib.hualdodo.interfaces;

public class Const {
    // Android中的类名
    public static final String ACTIVITY_CLASS = "android.app.Activity";

    // hualapp中的类名
    public static final String PKG = "com.huallib.hualdodo.";

    public static final String URI_HANDLER_CLASS =
            PKG + "core.UriHandler";
    public static final String URI_INTERCEPTOR_CLASS =
            PKG + "core.UriInterceptor";
    public static final String SERVICE_LOADER_CLASS =
            PKG + "service.ServiceLoader";
}
