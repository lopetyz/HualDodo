package com.huallib.common;

import android.app.Application;
import android.content.Context;

public class CommonApplication extends Application {

    private static CommonApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }
}
