package com.huallib.common.kotlinversion.util

import android.content.Context
import com.huallib.common.kotlinversion.CommonApplication

object AppRes {

    fun getAppContext(): Context {
        return CommonApplication.getAppContext()
    }

    private fun getAppExecutors(): AppExecutors {
        return CommonApplication.getAppExecutors()
    }

    fun runOnUiThread(runnable: Runnable) {
        getAppExecutors().mainThread().execute(runnable)
    }

    fun runOnUiThread(command: () -> Unit) {
        getAppExecutors().mainThread().execute(command)
    }
}