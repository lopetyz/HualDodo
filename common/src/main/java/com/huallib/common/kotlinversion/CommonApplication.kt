package com.huallib.common.kotlinversion

import android.app.Application
import com.huallib.common.kotlinversion.util.AppExecutors

class CommonApplication : Application() {

    companion object {
        lateinit var sInstance: CommonApplication
        lateinit var sAppExecutors: AppExecutors
        fun getAppContext() = sInstance
        fun getAppExecutors() = sAppExecutors
    }

    override fun onCreate() {
        super.onCreate()

        sInstance = this
        sAppExecutors = AppExecutors()
    }
}