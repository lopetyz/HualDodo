package com.huallib.common.network;

import com.huallib.common.network.interceptors.HostSelectionInterceptor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private Retrofit mRetrofit;
    private HostSelectionInterceptor mHostSelectionInterceptor = new HostSelectionInterceptor();

    private RetrofitClient() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);
        clientBuilder.addInterceptor(httpLoggingInterceptor);
        clientBuilder.addInterceptor(mHostSelectionInterceptor);
        OkHttpClient client = clientBuilder.build();

        mRetrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .build();
    }

    private static class InstanceHolder {
        private static RetrofitClient sInstance = new RetrofitClient();
    }

    public static RetrofitClient getInstance() {
        return InstanceHolder.sInstance;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public <T> T createService(Class<T> service) {
        mHostSelectionInterceptor.setHost("https://api.github.com/");
        T retrofitProxy = getInstance().mRetrofit.create(service);
        //再添加一层自定义的代理
        T customProxy = addCustomProxy(retrofitProxy);
        //返回这个嵌套的代理
        return customProxy;
    }

    public <T> T createService(Class<T> service, String url) {
        mHostSelectionInterceptor.setHost(url);
        T retrofitProxy = getInstance().mRetrofit.create(service);
        //再添加一层自定义的代理
        T customProxy = addCustomProxy(retrofitProxy);
        //返回这个嵌套的代理
        return customProxy;
    }

    @SuppressWarnings("unchecked")
    private <T> T addCustomProxy(T target) {
        CustomProxy customProxy = new CustomProxy();
        return (T) customProxy.newProxy(target);
    }
}
