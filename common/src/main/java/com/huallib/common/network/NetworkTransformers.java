package com.huallib.common.network;

import android.widget.Toast;

import com.huallib.common.util.AppRes;


import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleTransformer;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.HttpException;

public class NetworkTransformers {

    public static <T> SingleTransformer<T, T> schedulersTransformer() {
        return upstream -> upstream
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static <T> SingleTransformer<T, T> handleHttpException() {
        return upstream -> upstream.doOnError((Consumer<Throwable>) throwable -> {
            if (throwable instanceof HttpException) {
                HttpException httpException = (HttpException) throwable;
                Toast.makeText(AppRes.getApplicationContext(),
                        "code: " + httpException.code() + "---message: " + httpException.message(),
                        Toast.LENGTH_SHORT).show();

//                Log.e("hualtest", "1 code: " + httpException.code() + "---message: " + httpException.message());
            }
        });
    }

    public static <T> SingleTransformer<T, T> handleResponse() {
        return upstream -> upstream.doOnSuccess(result -> {
            Toast.makeText(AppRes.getApplicationContext(),
                    "success",
                    Toast.LENGTH_SHORT).show();
        });
    }
}
