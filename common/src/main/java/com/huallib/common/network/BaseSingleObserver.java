package com.huallib.common.network;

import android.widget.Toast;

import com.huallib.common.util.AppRes;

import io.reactivex.rxjava3.core.SingleObserver;
import retrofit2.HttpException;

public abstract class BaseSingleObserver<T> implements SingleObserver<T> {

    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;
                Toast.makeText(AppRes.getApplicationContext(),
                        "code: " + httpException.code() + "---message: " + httpException.message(),
                        Toast.LENGTH_SHORT).show();

//            Log.e("hualtest", "code: " + httpException.code() + "---message: " + httpException.message());
        }
    }
}
