package com.huallib.common.network.interceptors;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;

public class HostSelectionInterceptor implements Interceptor {
    private volatile HttpUrl mHost = HttpUrl.parse("");

    public void setHost(String str) {
        mHost = HttpUrl.parse(str);
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Builder newBuilder = request.newBuilder();
        HttpUrl url = request.url();
        if (!url.toString().contains(mHost.toString())) {
            newBuilder.url(url.newBuilder().scheme(mHost.scheme()).host(mHost.host()).port(mHost.port()).build());
        }
        return chain.proceed(newBuilder.build());
    }
}
