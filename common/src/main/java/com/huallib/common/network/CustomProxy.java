package com.huallib.common.network;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

/**
 * 自定义动态代理
 * 对retrofit service进行二次包装
 * 作为一个多重动态代理
 */
public class CustomProxy implements InvocationHandler {
    //被代理对象，在这里就是 Retrofit.create(service) 的返回值
    private Object mTarget;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String description = "proxy hechu";
        final String methodName = method.getName();

        if (methodName.equalsIgnoreCase("hechu")) {
            //将参数长度+1，作为新的参数数组
            args = Arrays.copyOf(args, (args.length + 1));
            //在新的参数数组末端加上 description
            args[args.length - 1] = description;

            //为了调用带 description 版本的方法，构造新的形参
            Class[] newParams = Arrays.copyOf(method.getParameterTypes(), (method.getParameterTypes().length + 1));
            //新的形参里，最后一个参数 from 是String类型的，这个必须声明，才能准确调用反射
            newParams[newParams.length - 1] = String.class;

            //找出新method对象，就是带 from 版本的那个方法
            method = mTarget.getClass().getDeclaredMethod(method.getName(), newParams);
        }

        return method.invoke(mTarget, args);
    }

    //在这里嵌套外层的动态代理
    public Object newProxy(Object target) {
        this.mTarget = target;
        return Proxy.newProxyInstance(this.getClass().getClassLoader(), target.getClass().getInterfaces(), this);
    }
}
