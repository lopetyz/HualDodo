package com.huallib.common.util;

import android.widget.Toast;

public class ToastUtil {

    public static void showShortToast(String msg) {
        Toast.makeText(AppRes.getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
