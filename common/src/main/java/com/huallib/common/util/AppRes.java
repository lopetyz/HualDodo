package com.huallib.common.util;

import android.content.Context;

import com.huallib.common.CommonApplication;

public class AppRes {

    public static Context getApplicationContext() {
        return CommonApplication.getAppContext();
    }
}
