package com.huallib.hualdodo.library;

import android.app.Activity;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class HomeActivityTools {
    public static void launch(Activity activity) {
        Class<?> clazz = activity.getClass();
        try {
            Log.e("hualtest", "bind:" + clazz.getName());
//            Class bindViewClass = Class.forName(clazz.getName() + "_KtViewBinding");
            Class<?> bindViewClass = Class.forName(clazz.getName() + "_HomeStart");
            Method method = bindViewClass.getMethod("startActivity", activity.getClass());
            method.invoke(bindViewClass.newInstance(), activity);
        } catch (ClassNotFoundException e) {
            Log.e("hualtest", "ClassNotFoundException: " + e.getMessage());
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            Log.e("hualtest", "IllegalAccessException: " + e.getMessage());
            e.printStackTrace();
        } catch (InstantiationException e) {
            Log.e("hualtest", "InstantiationException: " + e.getMessage());
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            Log.e("hualtest", "NoSuchMethodException: " + e.getMessage());
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            Log.e("hualtest", "InvocationTargetException: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
