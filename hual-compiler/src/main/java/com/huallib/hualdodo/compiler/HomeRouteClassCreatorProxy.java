package com.huallib.hualdodo.compiler;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

import java.util.HashMap;
import java.util.Map;

import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

public class HomeRouteClassCreatorProxy {

    private final String mBindingClassName;
    private final String mPackageName;
    private final TypeElement mTypeElement;
    private final Map<Integer, String> mRoutePaths = new HashMap<>();

    public HomeRouteClassCreatorProxy(Elements elementUtils, TypeElement classElement) {
        mTypeElement = classElement;
        PackageElement packageElement = elementUtils.getPackageOf(mTypeElement);
        String packageName = packageElement.getQualifiedName().toString();
        String className = mTypeElement.getSimpleName().toString();
        mPackageName = packageName;
        mBindingClassName = className + "_HomeStart";
    }

    public void putRoutePaths(int[] ids, String[] classNames) {
        if (ids.length != classNames.length) {
            throw new IllegalArgumentException("not match");
        }
        for (int i = 0; i < ids.length; i++) {
            mRoutePaths.put(ids[i], classNames[i]);
        }
    }

    public TypeSpec generateJavaCode() {
        TypeSpec bindingClass = TypeSpec.classBuilder(mBindingClassName)
                .addModifiers(Modifier.PUBLIC)
                .addMethod(generateMethods())
                .build();
        return bindingClass;

    }

    private MethodSpec generateMethods() {
        ClassName host = ClassName.bestGuess(mTypeElement.getQualifiedName().toString());
        ClassName intent = ClassName.get("android.content", "Intent");
        ClassName componentName = ClassName.get("android.content", "ComponentName");

        MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("startActivity")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .returns(void.class)
                .addParameter(host, "host");

        for (int id : mRoutePaths.keySet()) {
            String className = mRoutePaths.get(id);
            methodBuilder.addStatement("host.findViewById(" + id + ")" +
                    ".setOnClickListener(v -> {" +
                    "$T intent = new Intent();" +
                    "intent.setComponent(new $T(host.getPackageName(), \"" + className + "\"));" +
                    "host.startActivity(intent);" +
                    "})", intent, componentName);
        }

        return methodBuilder.build();
    }


    public String getPackageName() {
        return mPackageName;
    }
}
