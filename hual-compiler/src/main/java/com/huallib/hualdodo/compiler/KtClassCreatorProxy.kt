package com.huallib.hualdodo.compiler

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.TypeSpec
import javax.lang.model.element.TypeElement
import javax.lang.model.element.VariableElement
import javax.lang.model.util.Elements

class KtClassCreatorProxy constructor(elementUtils: Elements?, private val classElement: TypeElement) {

    private val packageName = elementUtils?.getPackageOf(classElement)?.qualifiedName.toString()
    private var bindingClassName: String = classElement.simpleName.toString() + "_KtViewBinding"

    private val variableElementMap: HashMap<Int, VariableElement> = HashMap<Int, VariableElement>()

    fun putElement(id: Int, element: VariableElement) {
        variableElementMap[id] = element
    }

    fun generateFile(): FileSpec {
        return FileSpec.builder(packageName, bindingClassName)
                .addType(TypeSpec.classBuilder(bindingClassName)
                        .addFunction(generateFun())
                        .build())
                .build()
    }

    private fun generateFun(): FunSpec {
        val host = ClassName.bestGuess(classElement.qualifiedName.toString())
        val builder = FunSpec.builder("bind")
                .addParameter("host", host)
        for (id in variableElementMap.keys) {
            val element = variableElementMap[id]
            val name = element?.simpleName.toString()
            builder.addStatement("host.$name = host.findViewById($id);")
        }
        return builder.build()
    }
}