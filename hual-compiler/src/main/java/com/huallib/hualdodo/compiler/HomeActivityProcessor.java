package com.huallib.hualdodo.compiler;

import com.google.auto.service.AutoService;
import com.huallib.hualdodo.annotation.HomeActivity;
import com.huallib.hualdodo.interfaces.Const;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.JavaFile;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Type;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;

@AutoService(Processor.class)
public class HomeActivityProcessor extends AbstractProcessor {

    private Messager mMessager;
    private Types mTypeUtils;
    private Elements mElementUtils;
    private Map<String, HomeClassCreatorProcessor> mProxyMap = new HashMap<>();

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        mMessager = processingEnvironment.getMessager();
        mTypeUtils = processingEnvironment.getTypeUtils();
        mElementUtils = processingEnvironment.getElementUtils();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return new HashSet<>(Collections.singletonList(HomeActivity.class.getCanonicalName()));
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        mMessager.printMessage(Diagnostic.Kind.NOTE, "HomeActivityProcessor processing...");
        mProxyMap.clear();

//        CodeBlock.Builder builder = CodeBlock.builder();
//        String hash = null;
        Set<? extends Element> elements = roundEnvironment.getElementsAnnotatedWith(HomeActivity.class);
        for (Element element : elements) {
            VariableElement variableElement = (VariableElement) element;
            TypeElement classElement = (TypeElement) variableElement.getEnclosingElement();
            String fullClassName = classElement.getQualifiedName().toString();
            //elements的信息保存到mProxyMap中
            HomeClassCreatorProcessor proxy = mProxyMap.get(fullClassName);
            if (proxy == null) {
                proxy = new HomeClassCreatorProcessor(mElementUtils, classElement);
                mProxyMap.put(fullClassName, proxy);
            }
            HomeActivity bindAnnotation = variableElement.getAnnotation(HomeActivity.class);
            int id = bindAnnotation.value();
//            Class<?> cls = bindAnnotation.cls();
//            proxy.putElement(new HomeClassCreatorProcessor.HomeStart(id, cls), variableElement);
            String className = bindAnnotation.className();
            proxy.putElement(new HomeClassCreatorProcessor.HomeStart(id, className), variableElement);
        }

        //通过javapoet生成
        for (String key : mProxyMap.keySet()) {
            HomeClassCreatorProcessor proxyInfo = mProxyMap.get(key);
            JavaFile javaFile = JavaFile.builder(proxyInfo.getPackageName(), proxyInfo.generateJavaCode())
                    .build();
            try {
                //　生成文件
                javaFile.writeTo(processingEnv.getFiler());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        mMessager.printMessage(Diagnostic.Kind.NOTE, "process finish ...");
        return true;
    }

    public TypeElement typeElement(String className) {
        return mElementUtils.getTypeElement(className);
    }


    public TypeMirror typeMirror(String className) {
        return typeElement(className).asType();
    }

    public boolean isConcreteType(Element element) {
        return element instanceof TypeElement && !element.getModifiers().contains(
                Modifier.ABSTRACT);
    }

    public boolean isConcreteSubType(Element element, String className) {
        return isConcreteType(element) && isSubType(element, className);
    }

    public boolean isConcreteSubType(Element element, TypeMirror typeMirror) {
        return isConcreteType(element) && isSubType(element, typeMirror);
    }

    public boolean isSubType(TypeMirror type, String className) {
        return type != null && mTypeUtils.isSubtype(type, typeMirror(className));
    }

    public boolean isSubType(Element element, String className) {
        return element != null && isSubType(element.asType(), className);
    }

    public boolean isSubType(Element element, TypeMirror typeMirror) {
        return element != null && mTypeUtils.isSubtype(element.asType(), typeMirror);
    }

    public boolean isActivity(Element element) {
        return isConcreteSubType(element, Const.ACTIVITY_CLASS);
    }

    public boolean isHandler(Element element) {
        return isConcreteSubType(element, Const.URI_HANDLER_CLASS);
    }

    public boolean isInterceptor(Element element) {
        return isConcreteSubType(element, Const.URI_INTERCEPTOR_CLASS);
    }

    public static String hash(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            return new BigInteger(1, md.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            return Integer.toHexString(str.hashCode());
        }
    }

    public CodeBlock buildHandler(boolean isActivity, Symbol.ClassSymbol cls) {
        CodeBlock.Builder b = CodeBlock.builder();
        if (isActivity) {
            b.add("$S", cls.className());
        } else {
            b.add("new $T()", cls);
        }
        return b.build();
    }

    public CodeBlock buildInterceptors(List<? extends TypeMirror> interceptors) {
        CodeBlock.Builder b = CodeBlock.builder();
        if (interceptors != null && interceptors.size() > 0) {
            for (TypeMirror type : interceptors) {
                if (type instanceof Type.ClassType) {
                    Symbol.TypeSymbol e = ((Type.ClassType) type).asElement();
                    if (e instanceof Symbol.ClassSymbol && isInterceptor(e)) {
                        b.add(", new $T()", e);
                    }
                }
            }
        }
        return b.build();
    }


}

