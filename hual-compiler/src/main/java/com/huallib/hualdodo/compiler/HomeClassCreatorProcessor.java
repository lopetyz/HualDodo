package com.huallib.hualdodo.compiler;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

import java.util.HashMap;
import java.util.Map;

import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.Elements;

public class HomeClassCreatorProcessor {
//    public static class HomeStart {
//        int id;
//        Class<?> cls;
//
//        public HomeStart(int id, Class<?> cls) {
//            this.id = id;
//            this.cls = cls;
//        }
//    }

    public static class HomeStart {
        int id;
        String cls;

        public HomeStart(int id, String cls) {
            this.id = id;
            this.cls = cls;
        }
    }

    private final String mBindingClassName;
    private final String mPackageName;
    private final TypeElement mTypeElement;
    private final Map<HomeStart, VariableElement> mVariableElementMap = new HashMap<>();

    public HomeClassCreatorProcessor(Elements elementUtils, TypeElement classElement) {
        mTypeElement = classElement;
        PackageElement packageElement = elementUtils.getPackageOf(mTypeElement);
        String packageName = packageElement.getQualifiedName().toString();
        String className = mTypeElement.getSimpleName().toString();
        mPackageName = packageName;
        mBindingClassName = className + "_HomeStart";
    }

    public void putElement(HomeStart homeStart, VariableElement element) {
        mVariableElementMap.put(homeStart, element);
    }

    public TypeSpec generateJavaCode() {
        TypeSpec bindingClass = TypeSpec.classBuilder(mBindingClassName)
                .addModifiers(Modifier.PUBLIC)
                .addMethod(generateMethods())
                .build();
        return bindingClass;

    }

    private MethodSpec generateMethods() {
        ClassName host = ClassName.bestGuess(mTypeElement.getQualifiedName().toString());
        ClassName intent = ClassName.get("android.content", "Intent");
        ClassName componentName = ClassName.get("android.content", "ComponentName");

        MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("startActivity")
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .returns(void.class)
                .addParameter(host, "host");

        for (HomeStart homeStart : mVariableElementMap.keySet()) {
            VariableElement element = mVariableElementMap.get(homeStart);
//            String name = element.getSimpleName().toString();
//            String type = element.asType().toString();
//            methodBuilder.addCode("host.findViewById(" + homeStart.id + ")" +
//                    ".setOnClickListener(v -> " +
//                    "    host.startActivity(new Intent(" + host + ", " + homeStart.cls + ".class);" +
//                    ")");
            methodBuilder.addStatement("host.findViewById(" + homeStart.id + ")" +
                    ".setOnClickListener(v -> {" +
                    "$T intent = new Intent();" +
                    "intent.setComponent(new $T(host.getPackageName(), \"" + homeStart.cls + "\"));" +
                    "host.startActivity(intent);" +
                    "})", intent, componentName);
        }
        return methodBuilder.build();
    }


    public String getPackageName() {
        return mPackageName;
    }
}
