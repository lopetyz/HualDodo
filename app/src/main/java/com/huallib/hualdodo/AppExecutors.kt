package com.huallib.hualdodo

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class AppExecutors private constructor(diskIO: Executor, networkIO: Executor, mainThread: Executor) {

    private var mDiskIO: Executor = diskIO
    private var mNetworkIO: Executor = networkIO
    private var mMainThread: Executor = mainThread

    constructor() : this(Executors.newSingleThreadExecutor(),
            Executors.newFixedThreadPool(3),
            MainThreadExecutor())

    fun getDiskIO(): Executor {
        return mDiskIO
    }

    fun networkIO(): Executor {
        return mNetworkIO
    }

    fun mainThread(): Executor {
        return mMainThread
    }
}

private class MainThreadExecutor : Executor {
    private val mainThreadHandler = Handler(Looper.getMainLooper())
    override fun execute(command: Runnable) {
        mainThreadHandler.post(command)
    }
}