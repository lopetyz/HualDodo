package com.huallib.hualdodo;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.huallib.hualdodo.annotation.HomeRoute;
import com.huallib.hualdodo.databinding.ActivityMainBinding;
import com.huallib.hualdodo.library.HomeActivityTools;

@HomeRoute(
        ids = {
                R.id.btn_go,
                R.id.btn_singalr,
                R.id.btn_jetpack,
                R.id.btn_camera,
                R.id.btn_worker,
                R.id.btn_coroutines
        },
        classNames = {
                "com.huallib.hualdodo.retrofit.RetrofitActivity",
                "com.huallib.hualdodo.signalr.SignalRActivity",
                "com.huallib.hualdodo.jetpack.JetpackActivity",
                "com.huallib.hualdodo.jetpack.camerax.CameraActivity",
                "com.huallib.hualdodo.jetpack.workers.WorkerActivity",
                "com.huallib.hualdodo.coroutines.CoroutinesActivity"
        }
)
public class MainActivity extends AppCompatActivity {

//    @HomeActivity(value = R.id.btn_go, className = "com.huallib.hualdodo.retrofit.RetrofitActivity")
//    Button btnGo;
//
//    @HomeActivity(value = R.id.btn_singalr, className = "com.huallib.hualdodo.signalr.SignalRActivity")
//    Button btnsingalr;
//
//    @HomeActivity(value = R.id.btn_jetpack, className = "com.huallib.hualdodo.jetpack.JetpackActivity")
//    Button btnJetpack;
//
//    @HomeActivity(value = R.id.btn_camera, className = "com.huallib.hualdodo.jetpack.camerax.CameraActivity")
//    Button btnCamera;
//
//    @HomeActivity(value = R.id.btn_worker, className = "com.huallib.hualdodo.jetpack.workers.WorkerActivity")
//    Button btnWorker;
//
//    @HomeActivity(value = R.id.btn_coroutines, className = "com.huallib.hualdodo.coroutines.CoroutinesActivity")
//    Button btnCoroutines;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        HomeActivityTools.launch(this);
    }
}
