package com.huallib.hualdodo.coroutines

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.huallib.hualdodo.databinding.ActivityCoroutinesBinding
import kotlinx.coroutines.*

class CoroutinesActivity : AppCompatActivity() {

    private val mainScope = MainScope()
    private var count = 0
    private lateinit var binding: ActivityCoroutinesBinding


    var job: Job? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCoroutinesBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        binding.tvCount.text = count.toString()
        binding.btnStart.setOnClickListener { start() }
        binding.btnStop.setOnClickListener { stop() }

//        job = GlobalScope.launch {
//            while (true) {
//                delay(1000)
//                count++
//                binding.tvCount.text = async {
//                    count.toString()
//                }.await()
//            }
//        }
    }

    private fun start() {
//        mainScope.launch {
//            while (true) {
//                delay(1000)
//                count++
//                binding.tvCount.text = async {
//                    count.toString()
//                }.await()
//            }
//        }

        job = GlobalScope.launch {
            while (true) {
                delay(1000)
                count++
                binding.tvCount.text = withContext(Dispatchers.Default) {
                    count.toString()
                }
            }
        }

        job?.start()
    }

    private fun stop() {
//        mainScope.cancel()

        job?.cancel()
    }
}
