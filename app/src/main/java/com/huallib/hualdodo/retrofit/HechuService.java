package com.huallib.hualdodo.retrofit;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HechuService {

    @GET("hechu")
    Single<Hechu> hechu(@Query("index") int index);

    @GET("hechu")
    Single<Hechu> hechu(@Query("index") int index, @Query("description") String description);

    @GET("hechu")
    Single<Hechu> hechuWithProxy(@Query("index") int index, @Query("description") String description);
}
