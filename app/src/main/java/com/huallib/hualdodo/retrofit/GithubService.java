package com.huallib.hualdodo.retrofit;

import com.huallib.common.network.BaseSingleObserver;

import java.util.List;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GithubService {

    @GET("users/{user}/repos")
    Call<List<Repo>> listRepos(@Path("user") String user);

    @GET("users/{user}/repos")
    Single<List<Repo>> listReposRxSingle(@Path("user") String user);

    @GET("users/{user}/repos")
    BaseSingleObserver<List<Repo>> listReposRxSingleObsever(@Path("user") String user);

    @GET("users/{user}/repos")
    Observable<List<Repo>> listReposRxObservable(@Path("user") String user);

    @GET("users/{user}/repos")
    Maybe<List<Repo>> listReposRxMabye(@Path("user") String user);

    @GET("users/{user}/repos")
    Completable listReposRxCompletable(@Path("user") String user);
}
