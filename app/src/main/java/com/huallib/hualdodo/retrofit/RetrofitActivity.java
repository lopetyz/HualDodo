package com.huallib.hualdodo.retrofit;

import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.huallib.common.network.NetworkTransformers;
import com.huallib.common.network.RetrofitClient;
import com.huallib.hualdodo.R;
import com.huallib.hualdodo.annotation.bindviews.BindView;
import com.huallib.hualdodo.library.BindViewTools;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class RetrofitActivity extends AppCompatActivity {

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @BindView(R.id.btn_test)
    Button mBtnTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrofit);
        BindViewTools.bind(this);

//        findViewById(R.id.btn_test).setOnClickListener(v -> queryRepoList());
        mBtnTest.setOnClickListener(v -> queryRepoList());
        findViewById(R.id.btn_host).setOnClickListener(v -> doHostInterceptor());
        findViewById(R.id.btn_custom_proxy).setOnClickListener(v -> doCustomProxy());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCompositeDisposable.dispose();
    }

    private void queryRepoList() {
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("https://api.github.com/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .build();
//
//        GithubService githubService = retrofit.create(GithubService.class);
        GithubService githubService = RetrofitClient.getInstance().createService(GithubService.class);

        doRxSingle(githubService);
//        doRxObservable(githubService);
//        doRxMaybe(githubService);
//        doRxCompletable(githubService);
    }

    private void doRxSingle(GithubService githubService) {
//        Disposable disposable = githubService.listReposRxSingle("lopetyzwwwww")
//                .compose(NetworkTransformers.schedulersTransformer())
//                .compose(NetworkTransformers.handleHttpException())
//                .subscribe(new Consumer<List<Repo>>() {
//                    @Override
//                    public void accept(List<Repo> repos) throws Exception {
//
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        HttpException httpException = (HttpException) throwable;
//                        Log.e("hualtest", "2 code: " + httpException.code() + "---message: " + httpException.message());
//                    }
//                });

//        Disposable disposable = githubService.listReposRxSingle("lopetyz")
//                .compose(NetworkTransformers.schedulersTransformer())
//                .compose(NetworkTransformers.handleHttpException())
//                .compose(NetworkTransformers.handleResponse())
//                .subscribe((repos, throwable) -> {
//
//                });

        Disposable disposable = githubService.listReposRxSingle("lopetyz")
                .compose(NetworkTransformers.schedulersTransformer())
                .compose(NetworkTransformers.handleHttpException())
                .compose(NetworkTransformers.handleResponse())
                .subscribe(repos -> {

                }, Throwable::printStackTrace);

        mCompositeDisposable.add(disposable);

//         githubService.listReposRxSingle("lopetyzwwwww")
//                .compose(NetworkTransformers.schedulersTransformer())
//                 .map(new Function<List<Repo>, List<Repo>>() {
//                     @Override
//                     public List<Repo> apply(List<Repo> repos) throws Exception {
//                         return null;
//                     }
//                 })
////                .compose(NetworkTransformers.handleHttpException())
//                .subscribe(new BaseSingleObserver<List<Repo>>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//
//                    }
//
//                    @Override
//                    public void onSuccess(List<Repo> repos) {
//
//                    }
//                });
    }

    private void doRxObservable(GithubService githubService) {
        final List<Disposable> disposable = new ArrayList<>();
        githubService.listReposRxObservable("lopetyz")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Repo>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable.add(d);
                    }

                    @Override
                    public void onNext(List<Repo> repos) {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        for (Disposable d : disposable) {
                            if (!d.isDisposed()) {
                                d.dispose();
                            }
                        }
                    }
                });
    }

    private void doRxMaybe(GithubService githubService) {
        Disposable disposable = githubService.listReposRxMabye("lopetyz")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Repo>>() {
                    @Override
                    public void accept(List<Repo> repos) throws Exception {

                    }
                });

        mCompositeDisposable.add(disposable);
    }

    private void doRxCompletable(GithubService githubService) {
        githubService.listReposRxCompletable("lopetyz")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }


    private void doHostInterceptor() {
        HechuService hechuService = RetrofitClient.getInstance().createService(HechuService.class, "http://192.168.1.127:8080/");
        Disposable disposable = hechuService.hechuWithProxy(1, "gogogogogogo")
                .compose(NetworkTransformers.schedulersTransformer())
                .compose(NetworkTransformers.handleHttpException())
                .compose(NetworkTransformers.handleResponse())
                .subscribe(repos -> {

                }, Throwable::printStackTrace);

        mCompositeDisposable.add(disposable);
    }

    private void doCustomProxy() {
        HechuService hechuService = RetrofitClient.getInstance().createService(HechuService.class, "http://192.168.1.127:8080/");
        Disposable disposable = hechuService.hechu(1)
                .compose(NetworkTransformers.schedulersTransformer())
                .compose(NetworkTransformers.handleHttpException())
                .compose(NetworkTransformers.handleResponse())
                .subscribe(repos -> {

                }, Throwable::printStackTrace);

        mCompositeDisposable.add(disposable);
    }
}
