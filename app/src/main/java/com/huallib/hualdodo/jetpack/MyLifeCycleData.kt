package com.huallib.hualdodo.jetpack

class MyLifeCycleData(
        private val index: Int,
        private val name: String) {

    fun getIndex(): Int {
        return index
    }

    fun getName(): String {
        return name
    }

}
