package com.huallib.hualdodo.jetpack.camerax

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.huallib.hualdodo.R
import com.huallib.hualdodo.databinding.ActivityCameraBinding

class CameraActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCameraBinding

    companion object {
        const val REQUEST_VIDEO_CAPTURE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)
        binding = ActivityCameraBinding.inflate(layoutInflater)

        binding.btnCamera1.setOnClickListener { dispatchTakeVideoIntent() }
    }

    private fun dispatchTakeVideoIntent() {

    }

//    fun setCamera(camera: Camera?) {
//        if (mCamera == camera) {
//            return
//        }
//
//        stopPreviewAndFreeCamera()
//
//        mCamera = camera
//
//        mCamera?.apply {
//            mSupportedPreviewSizes = parameters.supportedPreviewSizes
//            requestLayout()
//
//            try {
//                setPreviewDisplay(holder)
//            } catch (e: IOException) {
//                e.printStackTrace()
//            }
//
//            // Important: Call startPreview() to start updating the preview
//            // surface. Preview must be started before you can take a picture.
//            startPreview()
//        }
//    }
}
