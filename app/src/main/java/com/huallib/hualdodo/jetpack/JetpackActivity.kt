package com.huallib.hualdodo.jetpack

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.huallib.common.util.ToastUtil
import com.huallib.hualdodo.R
import com.huallib.hualdodo.annotation.bindviews.BindKtView
import com.huallib.hualdodo.databinding.ActivityJetpackBinding
import com.huallib.hualdodo.library.KtBindViewTools
import com.orhanobut.logger.Logger

class JetpackActivity : AppCompatActivity() {

    @BindKtView(R.id.btn_lifecycle)
    var mBtnLifeCycle: Button? = null

    private lateinit var binding: ActivityJetpackBinding

    private lateinit var myLifeCycleListener: MyLifeCycleListener

    private lateinit var mNotificationManager: NotificationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_jetpack)
//        BindViewTools.bind(this)
        KtBindViewTools.bind(this)
        binding = ActivityJetpackBinding.inflate(layoutInflater)

        myLifeCycleListener = MyLifeCycleListener(this, lifecycle) { myLifeCycleData ->
            ToastUtil.showShortToast("myLifeCycleData: ${myLifeCycleData.getIndex()}, ${myLifeCycleData.getName()}")
        }

        mNotificationManager = NotificationManager(this, lifecycle) { message ->
            ToastUtil.showShortToast("received: $message")
//            runOnUiThread { ToastUtil.showShortToast("received: $message") }
        }

        mBtnLifeCycle?.setOnClickListener {
            lifecycleTest()
            ToastUtil.showShortToast("mBtnLifeCycle toast")
        }

        binding.btnEnable.setOnClickListener { mNotificationManager.enable() }
        binding.btnDisable.setOnClickListener { mNotificationManager.disable() }
    }

    override fun onStart() {
        super.onStart()
        Logger.e("JetpackActivity onStart")
    }

    private fun lifecycleTest() {
        myLifeCycleListener.enable()
    }
}
