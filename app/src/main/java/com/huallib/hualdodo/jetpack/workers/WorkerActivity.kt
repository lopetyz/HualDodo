package com.huallib.hualdodo.jetpack.workers

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.huallib.hualdodo.R
import com.huallib.hualdodo.databinding.ActivityWorkerBinding
import com.orhanobut.logger.Logger
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.Request

class WorkerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWorkerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_worker)
        binding = ActivityWorkerBinding.inflate(layoutInflater)

        binding.btnWorker.setOnClickListener {
            //            doTest()
            doSimpleWorker()
        }
    }

    override fun onResume() {
        super.onResume()
    }

    private fun doTest() {
        GlobalScope.launch {
            val client = OkHttpClient()
            val request = Request.Builder()
                .url("https://api.github.com/users/lopetyz/repos")
                .build()

            val response = client.newCall(request).execute()
            Logger.e(response.body!!.string())
        }
    }

    private fun doSimpleWorker() {
        val simpleWorker = OneTimeWorkRequestBuilder<SimpleWorker>().build()
        WorkManager.getInstance(this).enqueue(simpleWorker)
    }

}
