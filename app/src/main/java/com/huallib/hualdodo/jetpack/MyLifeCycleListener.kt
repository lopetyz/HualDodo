package com.huallib.hualdodo.jetpack

import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.orhanobut.logger.Logger

class MyLifeCycleListener(
        private val context: Context,
        private val lifecycle: Lifecycle,
        private val callback: (MyLifeCycleData) -> Unit
) : LifecycleObserver {
    private var enabled = false
    private lateinit var myLifeCycleData: MyLifeCycleData

    init {
        lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun start() {
        Logger.e("MyLifeCycleListener onStart")
        myLifeCycleData = MyLifeCycleData(1, "鹤初")
    }

    fun enable() {
        enabled = true
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
            // connect if not connected

            callback(myLifeCycleData)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stop() {

    }
}
