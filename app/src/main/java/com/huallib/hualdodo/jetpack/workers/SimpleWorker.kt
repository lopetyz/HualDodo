package com.huallib.hualdodo.jetpack.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.orhanobut.logger.Logger
import okhttp3.OkHttpClient
import okhttp3.Request

class SimpleWorker(
        context: Context,
        workerParams: WorkerParameters
) : Worker(context, workerParams) {

    override fun doWork(): Result {
        val client = OkHttpClient()
        val request = Request.Builder()
                .url("https://api.github.com/users/lopetyz/repos")
                .build()

        val response = client.newCall(request).execute()
        Logger.e(response.body!!.string())

        return Result.success()
    }

}