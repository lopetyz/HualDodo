package com.huallib.hualdodo.jetpack

import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.huallib.hualdodo.HualDodoApplication
import com.huallib.hualdodo.signalr.OnMessageReceivedListener
import com.huallib.hualdodo.signalr.SignalRMessageCenter

class NotificationManager(
        private val context: Context,
        private val lifecycle: Lifecycle,
        private val callback: (String) -> Unit
) : LifecycleObserver {

    private var mEnabled = false

    private val mOnMessageReceivedListener = OnMessageReceivedListener { args ->
        (context.applicationContext as HualDodoApplication).appExecutors.mainThread().execute {
            if (mEnabled) {
                callback(args[1])
            } else {
                callback("")
            }
        }
    }

    init {
        lifecycle.addObserver(this)
    }

    fun enable() {
        mEnabled = true
    }

    fun disable() {
        mEnabled = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun addSignalRReceiver() {
        SignalRMessageCenter.getInstance().registerMessageReceivers(mOnMessageReceivedListener)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun removeSignalRReceiver() {
        SignalRMessageCenter.getInstance().unregisterMessageReceivers(mOnMessageReceivedListener)
    }
}