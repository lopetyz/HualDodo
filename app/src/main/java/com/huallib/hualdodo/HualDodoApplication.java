package com.huallib.hualdodo;

import com.huallib.common.CommonApplication;
import com.huallib.hualdodo.signalr.SignalRMessageCenter;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

public class HualDodoApplication extends CommonApplication {

    private AppExecutors mAppExecutors;

    @Override
    public void onCreate() {
        super.onCreate();

        Logger.addLogAdapter(new AndroidLogAdapter());

        mAppExecutors = new AppExecutors();
//        SignalRMessageCenter.getInstance().start();
    }

    public AppExecutors getAppExecutors() {
        return mAppExecutors;
    }
}
