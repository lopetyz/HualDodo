package com.huallib.hualdodo.signalr;

public interface OnMessageReceivedListener {
    void onMessageReceived(String... args);
}
