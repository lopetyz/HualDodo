package com.huallib.hualdodo.signalr;

import android.os.AsyncTask;

import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.orhanobut.logger.Logger;

public class SignalRMessageClient {

    public interface ISignalRClient {
        void onSignalRMessageReceived(String... args);
    }

    private HubConnection mHubConnection = HubConnectionBuilder.create("http://192.168.0.143:8088/hub").build();
    private ISignalRClient mSignalRClient;

    public SignalRMessageClient() {
        mHubConnection.on("MessageReceived", (user, msg) -> {
            Logger.e("user: " + user + "---msg: " + msg);
            mSignalRClient.onSignalRMessageReceived(user, msg);
        }, String.class, String.class);
    }

    public void setSignalRClient(ISignalRClient signalRClient) {
        mSignalRClient = signalRClient;
    }

    public void start() {
        new HubConnectionTask().execute(mHubConnection);
    }

    class HubConnectionTask extends AsyncTask<HubConnection, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(HubConnection... hubConnections) {
            try {
                HubConnection hubConnection = hubConnections[0];
                hubConnection.start().blockingAwait();
                return null;
            } catch (Exception e) {
                e.getStackTrace();
                try {
                    //断线重连
                    Logger.e("断线重连");
                    Thread.sleep(5000);
                    start();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
            return null;
        }
    }
}

