package com.huallib.hualdodo.signalr;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.huallib.common.util.ToastUtil;
import com.huallib.hualdodo.R;

public class SignalRActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signal_r);

//        SignalRMessageClient signalRMessageClient = new SignalRMessageClient();
//        signalRMessageClient.start();

        SignalRMessageCenter.getInstance().registerMessageReceivers(args ->
                runOnUiThread(() -> ToastUtil.showShortToast("user: " + args[0] + "---msg: " + args[1])));
    }
}
