package com.huallib.hualdodo.signalr;

import java.util.ArrayList;
import java.util.List;

public class SignalRMessageCenter implements SignalRMessageClient.ISignalRClient {

    private SignalRMessageClient mSignalRMessageClient;

    private List<OnMessageReceivedListener> mOnMessageReceivedListeners = new ArrayList<>();

    @Override
    public void onSignalRMessageReceived(String... args) {
        for (OnMessageReceivedListener receiver : mOnMessageReceivedListeners) {
            receiver.onMessageReceived(args);
        }
    }

    private static class InstanceHolder {
        private static final SignalRMessageCenter INSTANCE = new SignalRMessageCenter();
    }

    private SignalRMessageCenter() {
        mSignalRMessageClient = new SignalRMessageClient();
        mSignalRMessageClient.setSignalRClient(this);
    }

    public static SignalRMessageCenter getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public void start() {
        mSignalRMessageClient.start();
    }

    public void registerMessageReceivers(OnMessageReceivedListener listener) {
        mOnMessageReceivedListeners.add(listener);
    }

    public void unregisterMessageReceivers(OnMessageReceivedListener listener) {
        mOnMessageReceivedListeners.remove(listener);
    }
}
